<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\WebProfilerServiceProvider;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Silex\Provider\DoctrineServiceProvider;
use Provider\OAuthServiceProvider;

//use worldsailing\Isaf\provider\IsafOrmProvider;

$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app->register(new Sorien\Provider\PimpleDumpProvider());

//Register log service
$app->register(new MonologServiceProvider(), []);
//Register error log
$app->extend('monolog', function($monolog, $app) {
    $monolog->pushHandler( new StreamHandler($app['monolog.config']['error_log_file'], Logger::ERROR) );
    return $monolog;
});
//Register debug log on "dev" and "test"
if ((ENVIRONMENT === 'dev') || (ENVIRONMENT === 'test')){
    $app->extend('monolog', function($monolog, $app) {
        $monolog->pushHandler( new StreamHandler($app['monolog.config']['debug_log_file'], Logger::DEBUG) );
        return $monolog;
    });
}

//Template system definition.
$app->register(new TwigServiceProvider(), array(
    'twig.options'		=> array(
        'cache'			=> isset($app['twig.config']['cache']) ? $app['twig.config']['cache'] : false,
        'strict_variables' => true
    ),
    'twig.path'		   => array( $app['twig.config']['template_path'] )
));
//Register external twig functions
$app->extend('twig', function($twig, $app) {
    $twig->addFilter('json_decode', new \Twig_Filter_Function('json_decode'));
    $twig->addFilter('var_dump', new \Twig_Filter_Function('var_dump'));
    $twig->addFilter('get_object_vars', new \Twig_Filter_Function('get_object_vars'));
    $twig->addFilter('array_keys', new \Twig_Filter_Function('array_keys'));
    $twig->addFilter('current', new \Twig_Filter_Function('current'));
    return $twig;
});

//Profiler - Only for dev env
if (ENVIRONMENT === 'dev') {
    $app->register(new WebProfilerServiceProvider(), array(
        'profiler.cache_dir' => $app['profiler.config']['cache_dir'],
        'profiler.debug_toolbar' => $app['profiler.config']['debug_toolbar'],
        'profiler.debug_redirects' => $app['profiler.config']['debug_redirects'],
    ));
}

//Doctrine
/*
 * Register databases
 */
$app->register(new DoctrineServiceProvider, [
    'dbs.options' => $app['dbs.config']
]);

/*
 * Register World Sailing custom database ORM provider (Database name: isaf. Connection alias must be isaf in dbs.config)
 *
 */
//$app->register(new IsafOrmProvider());

/*
 * Validator
 */
$app->register(new Silex\Provider\ValidatorServiceProvider());

//OAuth service
$app->register( new OAuthServiceProvider(), []);

return $app;

