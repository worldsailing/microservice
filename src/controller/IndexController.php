<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Controller;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;

/**
 * Class IndexController
 * @package Controller
 */
class IndexController extends AbstractController  implements ControllerProviderInterface
{
    /**
     * @param Application $app
     * @return mixed
     */
    public function connect(Application $app )
    {
        parent::connect($app);
        $controller = $app['controllers_factory'];
        $controller->get("/", array( $this, 'index' ) )->bind( 'homepage' );
        return $controller;
    }

    /**
     * @param Application $app
     * @return mixed
     */
    public function index(Application $app )
    {
        return $app['twig']->render('index/index.html.twig', array());
    }
}
