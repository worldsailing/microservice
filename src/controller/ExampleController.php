<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Controller;

use worldsailing\Example\provider\ExampleServiceProvider;
use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;

use worldsailing\Api\response\example\ExampleEntity;
use worldsailing\Api\response\example\ExampleList;
use worldsailing\Common\Exception\WsServiceException;

/**
 * Class ExampleController
 * @package Controller
 */
class ExampleController extends AbstractResourceController implements ControllerProviderInterface
{
    /**
     * @param Application $app
     * @return mixed
     */
    public function connect(Application $app )
    {
        parent::connect($app);

        $controller = $app['controllers_factory'];
        /*
         * Register available methods
         */
        $controller->get("/", array( $this, 'index' ) )->bind( 'example_list' );
        $controller->get("/{id}/", array( $this, 'get' ) )->bind( 'example_form' );
        $controller->put("/{id}/", array( $this, 'put') )->bind( 'edit_example');

        $app->register(new ExampleServiceProvider(), []);

        return $controller;
    }

    /**
     * @param Application $app
     * @return mixed
     */
    public function index(Application $app )
    {
        $this->verifyToken($app);

        $model = $app['service.example']->modelFactory($app);

        $resultSet = $app['service.example']->getExampleList($model);

        $resultModel = new ExampleList('ExampleList', $resultSet->getData());

        return $app['output.list']($resultModel->map(), $resultSet->getCountAll());
    }


    /**
     * @param Application $app
     * @param $id
     * @return mixed
     */
    public function get(Application $app, $id)
    {
        $this->verifyToken($app);

        $model = $app['service.example']->modelFactory($app);

        $resultSet = $app['service.example']->getExampleById($model, $id); //183183

        $resultModel = new ExampleEntity('ExampleEntity', $resultSet->getData());

        return $app['output.entity']($resultModel->map());
    }


    /**
     * @param Application $app
     * @param $id
     * @return mixed
     */
    public function put(Application $app, $id)
    {
        $this->verifyToken($app);

        $model = $app['service.example']->modelFactory($app);

        parse_str(file_get_contents('php://input', false , null, -1 , $_SERVER['CONTENT_LENGTH'] ), $_PUT);

        try {
            $resultSet = $app['service.example']->setExampleData($model, new Request($_PUT), $id);

            $resultModel = new ExampleEntity('ExampleEntity', $resultSet->getData());

        } catch( WsServiceException $e) {
            return $app['output.entity']($e->__map(), $e->getCode());
        } catch(\Exception $e) {
            return $app['output.error']($e->getCode(), $e->getMessage());
        }

        return $app['output.entity']($resultModel->map(), 202);
    }

}

