<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

use Symfony\Component\Console\Input\InputInterface;

/**
 * Class Sample
 */
class Sample {

    /**
     * @param InputInterface $input
     */
    public static function doSomething(InputInterface $input) {
        echo "Sample command is done.\n";
        echo "some-option: " . $input->getOption('some-option') . "\n";
    }
}
