<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Provider;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

use OAuth2\HttpFoundationBridge\Response as BridgeResponse;

/**
 * Class OAuthServiceProvider
 * @package Provider
 */
class OAuthServiceProvider implements ServiceProviderInterface
{
    /**
     * @param Container $app
     */
    public function register(Container $app)
    {

        $app['service.oauth.server'] = function($app) {

            switch ($app['oauth.config']['storage']) {
                case 'pdo' :
                    $storage =new \OAuth2\Storage\Pdo(array('dsn' => $app['oauth.config']['connection']['dsn'],
                        'username' => $app['oauth.config']['connection']['user'],
                        'password' => $app['oauth.config']['connection']['password']));
                    break;
                default:
                    $storage =new \OAuth2\Storage\Pdo(array('dsn' => $app['oauth.config']['connection']['dsn'],
                        'username' => $app['oauth.config']['connection']['user'],
                        'password' => $app['oauth.config']['connection']['password']));
                    break;
            }

            // Pass a storage object or array of storage objects to the OAuth2 server class
            $server = new \OAuth2\Server($storage);

            //Add the "Client Credentials" grant type (it is the simplest of the grant types)
            $server->addGrantType(new \OAuth2\GrantType\ClientCredentials($storage));

            //Add the "Authorization Code" grant type (this is where the oauth magic happens)
            $server->addGrantType(new \OAuth2\GrantType\AuthorizationCode($storage));

            return $server;
        };
        $app['service.oauth.response'] = new BridgeResponse();

        return;
    }
}
