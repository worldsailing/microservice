<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Provider;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Symfony\Component\HttpFoundation\Response;
use worldsailing\Helper\WsHelper;

/**
 * Class OutputProvider
 * @package Providers
 */
class OutputProvider implements ServiceProviderInterface {

    /**
     * @param Container $app
     */
    public function register(Container $app) {
        $app['output.list'] = $app->protect(function ($rows, $count = 0, $code = 200, $headers = []) use ($app) {
            $data = ['result' => $rows, 'records_in_scope' => $count];
            if ($app['debug'] === true && $app['debug.visual'] === true) {
                return $app['twig']->render('index/list.html.twig', array('content' => json_encode($data['result'])));
            } else {
                return $app->json($data, $code, $headers);
            }
        });

        $app['output.entity'] = $app->protect(function ($data, $code = 200, $headers = []) use ($app) {
            if ($app['debug'] === true && $app['debug.visual'] === true) {
                return $app['twig']->render('index/entity.html.twig', array('content' => json_encode($data)));
            } else {
                return $app->json($data, $code, $headers);
            }
        });

        $app['output.dump'] = $app->protect(function ($data, $code = 200, $headers = []) use ($app) {
            if ($app['debug'] === true && $app['debug.visual'] === true) {
                return $app['twig']->render('index/vardump.html.twig', array('content' => $data));
            } else {
                return $app->json($data, $code, $headers);
            }
        });

        $app['output.error'] = $app->protect(function ($code, $message = null, $headers = []) use ($app) {
            if (! $code) {
                $code = 500;
            }
            $message = ($message && strlen($message) > 0) ? $message : WsHelper::getResponseMessageByCode($code);
            if ($app['debug'] === true && $app['debug.visual'] === true) {
                $templates = array(
                    '/errors/'.$code.'.html.twig',
                    '/errors/'.substr($code, 0, 2).'x.html.twig',
                    '/errors/'.substr($code, 0, 1).'xx.html.twig',
                    '/errors/default.html.twig',
                );
                return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code, 'message' =>$message)), $code);
            } else {
                return new Response(json_encode(['error' => $message]), $code, $headers);
            }
        });

        return;
    }

}

