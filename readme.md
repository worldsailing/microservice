# worldsailing SkeletonService 

### PHP library description
Skeleton microservice for Silex based World Sailing microservices.

### Create a new service
1. Fork master branch of this repository.
2. Remove unnecessary bundles.
3. Add other bundles if you need.
4. Do custom application logic. 
5. Do not merge back your customs into this repository.
  
### Based on Silex skeleton

````
$ composer create-project fabpot/silex-skeleton path/to/install "~2.0"
````

### What's inside?

* `ValidatorServiceProvider` - Form validation
* `TwigServiceProvider` - Twig template engine
* `WebProfilerServiceProvider` - Debugging
* `MonologServiceProvider` - Lgging
* `Oauth2` - Oauth2 protocol provider
* `Doctrine ORM` - ORM based data provider

See Silex [complete documentation](http://silex.sensiolabs.org/doc/1.3/)
 
### License

All right reserved
