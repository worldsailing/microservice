<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

define( 'PATH_WEB', __DIR__ );
define( 'PATH_ROOT', realpath(PATH_WEB . '/..') );
define( 'PATH_CACHE', PATH_ROOT . '/var/cache' );
define( 'PATH_LOG', PATH_ROOT . '/var/log' );

define( 'PATH_VENDOR', PATH_ROOT . '/vendor' );
define( 'PATH_SRC', PATH_ROOT . '/src' );
define( 'PATH_LOCAL', PATH_SRC . '/localisation' );
define( 'PATH_TEMPLATES', PATH_SRC . '/template' );

define( 'ENVIRONMENT',  getenv('SYMFONY_ENV') ?: 'prod' ); //dev, test, prod

require_once PATH_VENDOR . '/autoload.php';

use Silex\Application;
use Symfony\Component\Debug\Debug;
use worldsailing\Helper\WsHelper;

if (ENVIRONMENT == 'dev') {
    Debug::enable();
}
WsHelper::setEnv(ENVIRONMENT);

$app = new Application();

require PATH_ROOT . '/config/' . ENVIRONMENT . '/config.php';

require PATH_SRC . '/app.php';

require PATH_SRC . '/routes.php';

$app->run();
